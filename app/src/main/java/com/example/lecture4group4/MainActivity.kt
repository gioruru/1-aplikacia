package com.example.lecture4group4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init();
    }

    private fun init() {
        loginBtn.setOnClickListener {
            if (emailView.text.toString().isEmpty() || passView.text.toString().isEmpty() ) {
                Toast.makeText(this, "fill all fields", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "you logged in!", Toast.LENGTH_SHORT).show();
            }
        }

    }
}
